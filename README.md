# TASK MANAGER

Console application for task list.

## DEVELOPER INFO

NAME:   Valentina Ushakova  
E-MAIL: vushakova@tsconsulting.com  

## SOFTWARE

* JDK 15.0.1
* Windows 10

## HARDWARE

* RAM 16Gb
* CPU i5
* HDD 128Gb

## RUN PROGRAM

    java -jar ./task-manager.jar

## BUILD PROGRAM  

    mvn clean install

## FUTURE SCOPE

Use Maven for project building.

## SCREENSHOTS

[Screenshot URL.](https://drive.google.com/drive/folders/1UV0PWC-IzKMcP0a7PmGVFfTD8sRLysh-?usp=sharing)  

[Screenshot URL: Build with MAVEN.](https://drive.google.com/drive/folders/1R3-1-796EV9gy4eGo_rWvtQChrNA1suN?usp=sharing)  

[Screenshot URL: Command handling.](https://drive.google.com/drive/folders/1V2YUMUeaPmK1fhQZiQBesXmb73p_63es?usp=sharing) 
